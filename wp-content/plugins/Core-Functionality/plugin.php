<?php
/**
 * Plugin Name: Core Functionality
 * Description: This contains all your site's core content based functionality so that it is theme independent.
 * Author: Noah Kramer, based on Bill Erickson's core functionality plugin
 */

// Plugin Directory 
define( 'BE_DIR', dirname( __FILE__ ) );
 
// Post Types
include_once( BE_DIR . '/lib/functions/post-types.php' );

// Taxonomies 
include_once( BE_DIR . '/lib/functions/taxonomies.php' );

// Editor Style Refresh
include_once( BE_DIR . '/lib/functions/editor-style-refresh.php' );

// General
include_once( BE_DIR . '/lib/functions/general.php' );

 add_theme_support( 'be-events-calendar', array('event-category','recurring-events') );