<?php

/****************************************************************
General Functions 
****************************************************************/


// Use shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );

// Wordpress Email Obfuscation as shortcode. (usage:  [email]your@email.com[/email] ).
function nk_email_encode_function( $atts, $content ){
  return '<a href="mailto:'.antispambot($content).'">'.antispambot($content).'</a>';}
  add_shortcode( 'email', 'nk_email_encode_function' );


/**
 * Customize Admin Bar Items
 * @link http://wp-snippets.com/addremove-wp-admin-bar-links/
 */
function nk_admin_bar_items() {
	global $wp_admin_bar;
	//$wp_admin_bar->remove_menu( 'new-link', 'new-content' );
  //$wp_admin_bar->remove_menu( 'new-media', 'new-content' );
  //$wp_admin_bar->remove_menu( 'new-page', 'new-content' );
  //$wp_admin_bar->remove_menu( 'new-user', 'new-content' );
  //$wp_admin_bar->remove_menu('wpseo-menu'); //removes WP-SEO 
}
add_action( 'wp_before_admin_bar_render', 'nk_admin_bar_items' );


/**
 * Customize Menu Order
 *
 * @param array $menu_ord. Current order.
 * @return array $menu_ord. New order.
 *
 */
function nk_custom_menu_order( $menu_ord ) {
	if ( !$menu_ord ) return true;
	return array(
		'index.php', // this represents the dashboard link
		'edit.php?post_type=page', //the page tab
		'edit.php', //the posts tab
		'upload.php', // the media manager
    'edit-comments.php', // the comments tab
    );
}
//add_filter( 'custom_menu_order', 'nk_custom_menu_order' );
//add_filter( 'menu_order', 'nk_custom_menu_order' );

/**
 * Pretty Printing
 * 
 * @author Chris Bratlien
 *
 * @param mixed
 * @return null
 */
function be_pp( $obj, $label = '' ) {  

	$data = json_encode(print_r($obj,true));
    ?>
    <style type="text/css">
      #bsdLogger {
      position: absolute;
      top: 30px;
      right: 0px;
      border-left: 4px solid #bbb;
      padding: 6px;
      background: white;
      color: #444;
      z-index: 999;
      font-size: 1.25em;
      width: 400px;
      height: 800px;
      overflow: scroll;
      }
    </style>    
    <script type="text/javascript">
      var doStuff = function(){
        var obj = <?php echo $data; ?>;
        var logger = document.getElementById('bsdLogger');
        if (!logger) {
          logger = document.createElement('div');
          logger.id = 'bsdLogger';
          document.body.appendChild(logger);
        }
        ////console.log(obj);
        var pre = document.createElement('pre');
        var h2 = document.createElement('h2');
        pre.innerHTML = obj;
 
        h2.innerHTML = '<?php echo addslashes($label); ?>';
        logger.appendChild(h2);
        logger.appendChild(pre);      
      };
      window.addEventListener ("DOMContentLoaded", doStuff, false);
 
    </script>
    <?php
}

// Disable WPSEO columns on edit screen 
add_filter( 'wpseo_use_page_analysis', '__return_false' );